def connect(arreglo):
    costo = 0
    while(len(arreglo)>1):
        x = min(arreglo)
        arreglo.remove(x)
        y = min(arreglo)
        arreglo.remove(y)
        suma = x+y
        costo += suma
        arreglo.append(suma)
    return costo

l = [4,3,2,6]
print(connect(l))
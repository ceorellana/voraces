import operator

def rearrange(cadena):
    unicos = set(cadena)
    frecuencia = {}
    for letra in unicos:
        frecuencia[letra] = cadena.count(letra)
    cadena = ''
    anterior = ''
    while (len(frecuencia.keys()) != 0):
        letra = max(frecuencia.items(), key=operator.itemgetter(1))[0]
        if (letra == anterior):
            keyValue = (letra,frecuencia[letra])
            del frecuencia[letra]
            if (len(frecuencia.keys()) == 0):
                print ("Not possible")
                return
            letra = max(frecuencia.items(), key=operator.itemgetter(1))[0]
            frecuencia[keyValue[0]] = keyValue[1]
        cadena += letra
        frecuencia[letra] -= 1
        if (frecuencia[letra] == 0):
            del frecuencia[letra]
        anterior = letra
    print(cadena)

cadena = "aaaeeee"
rearrange(cadena)
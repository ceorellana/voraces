def plataformas(arrival,departure):
    arrDep = []
    for i in range(len(arrival)):
        arrDep.append(arrival[i])
        arrDep.append(departure[i])
    horasAnalizadas = 0
    maxPlataformas = 0
    tmpPlataformas = 0
    while (horasAnalizadas!=len(arrDep)):
        hora = min(arrDep)
        pos = arrDep.index(hora)
        if (pos%2==0): #Es decir, si es una hora de arrivo
            tmpPlataformas += 1
            if (tmpPlataformas > maxPlataformas):
                maxPlataformas = tmpPlataformas
        else:
            tmpPlataformas -= 1
        horasAnalizadas += 1
        arrDep[pos] = '99:99' #Mantiene el tamaño del arreglo intacto y deja de tomar en cuenta la hora analizada
    return maxPlataformas

#Para que el algoritmo funcione, es necesario que las horas se encuentre en formato de 24h y que se coloque un 0 al comienzo
# de las horas menores a 10
arrivo = ['09:00','09:40','09:50','11:00','15:00','18:00']
partida = ['09:10','12:00','11:20','11:30','19:00','20:00']

print(plataformas(arrivo,partida))